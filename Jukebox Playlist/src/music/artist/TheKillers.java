package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheKillers {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheKillers() {
    }
    
    public ArrayList<Song> getTheKillersSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Mr. Brightside", "The Killers");             //Create a song
         Song track2 = new Song("Read My Mind", "The Killers");         //Create another song
         Song track3 = new Song("The Man", "The Killers");					//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for The Killers
         this.albumTracks.add(track2);                                          //Add the second song to song list for The Killers 
         this.albumTracks.add(track3);											//Add the third song to song list for The Killers
         return albumTracks;                                                    //Return the songs for The Killers in the form of an ArrayList
    }
}
