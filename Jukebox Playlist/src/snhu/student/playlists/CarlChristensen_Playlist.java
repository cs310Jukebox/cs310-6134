package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class CarlChristensen_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
		/* Creates data structure to store songs in a playlist. */
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		
		/* Grabs songs from Green Day */
		ArrayList<Song> GreenDayTracks = new ArrayList<Song>();
	    GreenDay gd = new GreenDay();
	    GreenDayTracks = gd.getGreenDaySongs();
	    
	    /* Grabs songs from Volbeat. */
	    ArrayList<Song> VolbeatTracks = new ArrayList<Song>();
	    Volbeat v = new Volbeat();
	    VolbeatTracks = v.getVolbeatSongs();
	    
		/* Adds all songs to playlist.*/
		playlist.add(GreenDayTracks.get(0));
		playlist.add(GreenDayTracks.get(1));
		playlist.add(VolbeatTracks.get(0));
		playlist.add(VolbeatTracks.get(1));
		playlist.add(VolbeatTracks.get(2));
		
		/* Returns playlist. */
	    return playlist;
	}
}