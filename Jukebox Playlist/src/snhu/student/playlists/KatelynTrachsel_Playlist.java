package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class KatelynTrachsel_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	/* Creates data structure to store songs in a playlist. */
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	//Grab songs from CRJ playlist.
	ArrayList<Song> CarlyRaeJepsenTracks = new ArrayList<Song>();
    CarlyRaeJepsen = new CarlyRaeJepsen();
    CarlyRaeJepsenTracks = CarlyRaeJepsen.getCarlyRaeJepsenSongs();
	//Adds CRJ songs.
	playlist.add(CarlyRaeJepsenTracks.get(0));
	playlist.add(CarlyRaeJepsenTracks.get(1));
	playlist.add(TheKillersTracks.get(2));
	//Grabs songs from the Killers playlist.
    TheKillers TheKillersBand = new TheKillers();
	ArrayList<Song> TheKillersTracks = new ArrayList<Song>();
    TheKillersTracks = TheKillersBand.getTheKillersSongs();
	//Adds the Kllers songs.
	playlist.add(TheKillersTracks.get(0));
	playlist.add(TheKillersTracks.get(1));
	
    return playlist;
	}
}
