package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import snhu.jukebox.playlist.Student;
import snhu.jukebox.playlist.StudentList;
import snhu.student.playlists.*;


public class StudentTest {

	//Test the list of student names and ensure we get back the name value we expect at the correct/specific point in the list
	@Test
	public void testGetStudentNameList1() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent1Name", studentNames.get(0));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test
	public void testGetStudentNameList2() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent2Name", studentNames.get(1));							//test case to see if the second value contains the name we expect
	}
	
	//Added test for Will Medrano Gutshall
	@Test
	public void testGetStudentNameList2() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("WillMedranoGutshall", studentNames.get(2));					    //test case to see if the third value contains the name we expect
	}

	
	
/*
 * Commenting out this test as it fails
 * Larry McRoberts
 */
//	@Test
//	public void testGetStudentNameList3() {
//		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
//		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
//		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
//		assertEquals("TestBrianRussell", studentNames.get(2));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
//	}

	
	@Test
	public void testGetWillMedranoGutshallProfile() {
		WillMedranoGutshall_Playlist testWillMedranoPlaylist = new WillMedranoGutshall_Playlist();						//instantiating the variable for a specific student
		Student WillMedranoGutshall = new Student("WillMedranoGutshall", testWillMedranoGutshallPlaylist.StudentPlaylist());		//creating populated student object
		assertEquals("WillMedranoGutshall", TestStudent1.getName());											//test case pass/fail line - did the name match what you expected?

	public void testGetStudentNameList3() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestBrianRussell", studentNames.get(4));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test
	public void testGetStudentNameList123() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("CarlChristensen", studentNames.get(3));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test
	public void testGetStudentNameList5() {

		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("LarryMcRoberts", studentNames.get(7));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test																				
	public void testGetHannahHollee() {													
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("HannahHollee", studentNames.get(5));								//test case for pass/fail. Set index to 5 as 
	}

	@Test
	public void testGetStudentNameList8() {
		List<String> studentNames = new ArrayList<String>();
		StudentList studentList = new StudentList();
		studentNames = studentList.getStudentsNames();
		assertEquals("AmandaKiefer", studentNames.get(10));
	}
	
	@Test
	public void testGetStudentNameListLHoo() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Li Siang Hoo", studentNames.get(11));							//test case to see if the third value contains the name we expect (TestStudentLHoo)
	}
	
	@Test
	public void testGetStudentNameList9() {
		List<String> studentNames = ArrayList<String>();                           //create variable for student list of names
		StudentList studentList = new StudentList();                               //instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();                             //populate the studentNames list with the actual values in the StudentsList object
		assertEquals("ChelseaLong", studentNames.get(12));                         //test case for pass/fail
	}

	//Module 6 Test Case Area
	//Test each student profile to ensure it can be retrieved and accessed
	@Test
	public void testGetStudent1Profile() {
		TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();						//instantiating the variable for a specific student
		Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());		//creating populated student object
		assertEquals("TestStudent1", TestStudent1.getName());											//test case pass/fail line - did the name match what you expected?
	}
	
	@Test
	public void testGetStudent2Profile() {
		TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
		Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
		assertEquals("TestStudent2", TestStudent2.getName());
	}
	@Test
	public void testGetHannahHolleeProfile() {
		HannahHollee_Playlist testHannahHolleePlaylist = new HannahHollee_Playlist();					//instantiating the variable for a specific student
		Student HannahHollee = new Student("Hannah Hollee", testHannahHolleePlaylist.StudentPlaylist());//creating populated student object
		assertEquals("Hannah Hollee", HannahHollee.getName());											//test case pass/fail line
	}
	
	//Add the unit test for Li Siang Hoo profile
	@Test
	public void testGetLHooProfile() {
		LiSiangHoo_Playlist liSiangHooPlaylist = new LiSiangHoo_Playlist();
		Student LiSiangHoo = new Student("Li Siang Hoo", liSiangHooPlaylist.StudentPlaylist());
		assertEquals("Li Siang Hoo", LiSiangHoo.getName());
	}
}
