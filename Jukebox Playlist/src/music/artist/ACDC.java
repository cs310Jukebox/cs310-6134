package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class ACDC {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ACDC() {
    }
    
    public ArrayList<Song> getACDCSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Thunderstruck", "ACDC");                       //Create a song
         Song track2 = new Song("Highway to Hell", "ACDC");                     //Create another song
         Song track3 = new Song("Back in Black", "ACDC");						//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for ACDC
         this.albumTracks.add(track2);                                          //Add the second song to song list for ACDC 
         this.albumTracks.add(track3);											//Add the third song to song list for ACDC
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
