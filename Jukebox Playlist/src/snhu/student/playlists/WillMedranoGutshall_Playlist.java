package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class WillMedranoGutshall_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> boneThugsTracks = new ArrayList<Song>();
    BoneThugs boneThugs = new BoneThugs();
	
    boneThugsTracks = boneThugs.getBoneThugsSongs();
	
	playlist.add(boneThugsTracks.get(0));
	playlist.add(boneThugsTracks.get(1));
	playlist.add(boneThugsTracks.get(2));
	
	
    MachineGunKelley machineGunKelley = new MachineGunKelley();
	ArrayList<Song> machineGunKelleyTracks = new ArrayList<Song>();
    machineGunKelleyTracks = machineGunKelley.getMachineGunKelleySongs();
	
	playlist.add(machineGunKelleyTracks.get(0));
	playlist.add(machineGunKelleyTracks.get(1));
	playlist.add(machineGunKelleyTracks.get(2));
	
	//Added Playlist from someone else's music: Green Day
	GreenDay greenDay = new GreenDay();
	ArrayList<Song> greenDayTracks = new ArrayList<Song>();
    greenDayTracks = greenDay.getGreenDaySongs();
	
	playlist.add(greenDayTracks.get(0));
	playlist.add(greenDayTracks.get(1));
	
	return playlist;
	}
}


