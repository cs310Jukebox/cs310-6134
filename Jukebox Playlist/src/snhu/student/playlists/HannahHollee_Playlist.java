package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class HannahHollee_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	// create new structure to store songs 
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> gregLaswellTracks = new ArrayList<Song>();
    GregLaswell gregLaswell = new GregLaswell();
	
	// gets songs and add to playlist
    gregLaswellTracks = gregLaswell.getGregLaswellSongs();
	
	playlist.add(gregLaswellTracks.get(0));
	playlist.add(gregLaswellTracks.get(1));
	
	// create new structure to store songs
    HeyMarseilles heyMarseilles = new HeyMarseilles();
	ArrayList<Song> heyMarseillesTracks = new ArrayList<Song>();
    heyMarseillesTracks = heyMarseilles.getHeyMarseillesSongs();
	
	// gets songs and add to playlist
	playlist.add(heyMarseillesTracks.get(0));
	playlist.add(heyMarseillesTracks.get(1));
	playlist.add(heyMarseillesTracks.get(2));
	
	// add playlist from another student
	MachineGunKelley machineGunKelley = new MachineGunKelley();
	ArrayList<Song> machineGunKelleyTracks = new ArrayList<Song>();
    machineGunKelleyTracks = machineGunKelley.getMachineGunKelleySongs();
	
	playlist.add(machineGunKelleyTracks.get(0));
	playlist.add(machineGunKelleyTracks.get(1));
	playlist.add(machineGunKelleyTracks.get(2));
	
    return playlist;
	}
}