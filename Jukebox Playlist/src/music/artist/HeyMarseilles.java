package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class HeyMarseilles {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public HeyMarseilles() {
    }
    
    public ArrayList<Song> getHeyMarseillesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Cities", "Hey Marseilles");                    //Create a song
         Song track2 = new Song("Bright Stars Burning", "Hey Marseilles");      //Create another song
         Song track3 = new Song("Eyes On You", "Hey Marseilles");
         this.albumTracks.add(track1);                                          //Add the first song to song list 
         this.albumTracks.add(track2);                                          //Add the second song to song list 
         this.albumTracks.add(track3);
         return albumTracks;                                                    //Return the songs in the form of an ArrayList
    }
}