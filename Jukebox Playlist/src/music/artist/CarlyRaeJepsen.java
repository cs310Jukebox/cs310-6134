package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class CarlyRaeJepsen {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public CarlyRaeJepsen() {
    }
    
    public ArrayList<Song> getCarlyRaeJepsenSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Run Away With Me", "Carly Rae Jepsen");             //Create a song
         Song track2 = new Song("Boy Problems", "Carly Rae Jepsen");         //Create another song
         Song track3 = new Song("Roses", "Carly Rae Jepsen");					//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Carly Rae Jepsen
         this.albumTracks.add(track2);                                          //Add the second song to song list for Carly Rae Jepsen 
         this.albumTracks.add(track3);											//Add the third song to song list for Carly Rae Jepsen
         return albumTracks;                                                    //Return the songs for Carly Rae Jepsen in the form of an ArrayList
    }
}
