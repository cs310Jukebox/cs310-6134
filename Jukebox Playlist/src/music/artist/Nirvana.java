package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Nirvana {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Nirvana() {
    }

	public ArrayList<Song> getNirvanaSongs() {
		
   	 	albumTracks = new ArrayList<Song>();                                   
   	 	Song track1 = new Song("Smells Like Teen Spirit", "Nirvana");             
        Song track2 = new Song("Come As You Are", "Nirvana");         
        this.albumTracks.add(track1);                                          
        this.albumTracks.add(track2);                                          
        return albumTracks;      
	}
}