package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BeeGees {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BeeGees() {
    }
    
    public ArrayList<Song> getBeeGeesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("How Deep Is Your Love", "BeeGees");            //Create a song
         Song track2 = new Song("Love You Inside Out", "BeeGees");		        //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the BeeGees
         this.albumTracks.add(track2);                                          //Add the second song to song list for the BeeGees 
         return albumTracks;                                                    //Return the songs for the BeeGees in the form of an ArrayList
    }
}
