package music.artist;

// This Playlist Added by Will Medrano Gutshall


import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BoneThugs {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BoneThugs() {
    }
    
    public ArrayList<Song> getBoneThugsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Cleveland is The City", "Bone Thugs-N-Harmony");             //Create a song
         Song track2 = new Song("Cleveland Rocks", "Bone Thugs-N-Harmony");              //Create 2nd song
         Song track3 = new Song("Thuggish Ruggish Bone", "Bone Thugs-N-Harmony");             //Create 3rd song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Bone Thugs
         this.albumTracks.add(track2);                                          //Add the second song to song list for Bone Thugs 
         this.albumTracks.add(track3);                                          //Add the third song to song list for Bone Thugs 
         return albumTracks;                                                    //Return the songs for Bone Thugs in the form of an ArrayList
    }
}
