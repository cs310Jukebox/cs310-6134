package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class MichaelSwift_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	//Structure for Queen 
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> QueenTracks = new ArrayList<Song>();
    Queen queen = new Queen();
	
    //Obtain songs for playlist from Queen class
    QueenTracks = queen.getQueenSongs();
	
	playlist.add(QueenTracks.get(0));
	playlist.add(QueenTracks.get(1));
	
	//Structure for ACDC band
    ACDC ACDCband = new ACDC();
	ArrayList<Song> ACDCTracks = new ArrayList<Song>();
    ACDCTracks = ACDCband.getACDCSongs();
	
    //Obtain songs for playlist from ACDC class
	playlist.add(ACDCTracks.get(0));
	playlist.add(ACDCTracks.get(1));
	playlist.add(ACDCTracks.get(2));
	
	//Structure for Nirvana
	Nirvana NirvanaBand = new Nirvana();
	ArrayList<Song> NirvanaTracks = new ArrayList<Song>();
	NirvanaTracks = NirvanaBand.getNirvanaSongs();
	
	//Obtain songs from Nirvana class
	playlist.add(NirvanaTracks.get(0));
	playlist.add(NirvanaTracks.get(1));
    
	//Structure for BillyJoel
	BillyJoel BillyJoelBand = new BillyJoel();
	ArrayList<Song> BillyJoelTracks = new ArrayList<Song>();
	BillyJoelTracks = BillyJoelBand.getBillyJoelSongs();
	
	//Obtain songs from BillyJoel class
	playlist.add(BillyJoelTracks.get(0));
	playlist.add(BillyJoelTracks.get(1));
			
	return playlist;
	}
}
