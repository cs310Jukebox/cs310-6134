package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BreakingBenjamin {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BreakingBenjamin() {
    }
    
    public ArrayList<Song> getBreakingBenjaminSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Firefly", "Breaking Benjamin");            	//Create first song
         Song track2 = new Song("Ashes of Eden", "Breaking Benjamin");         	//Create second song
         Song track3 = new Song("Simple Design", "Breaking Benjamin");			//Create third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Breaking Benjamin
         this.albumTracks.add(track2);                                          //Add the second song to song list for Breaking Benjamin
         this.albumTracks.add(track3);											//Add the third song to song list for Breaking Benjamin
         return albumTracks;                                                    //Return the songs for Breaking Benjamin in the form of an ArrayList
    }
}
