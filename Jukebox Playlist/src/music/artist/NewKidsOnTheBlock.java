package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/**
 * NewKidsOnTheBlock.java
 * 
 * This class represent the New Kids On the Block
 * song lists
 * 
 * @author Li Siang Hoo
 *
 */

public class NewKidsOnTheBlock {
	
	ArrayList<Song> albumTracks; // ArrayList of album tracks 
    String albumTitle;			 // Album title
    
    public NewKidsOnTheBlock() {
    }
    
    /**
	 * @return ArrayList
   	 * This returns to list of storing the Song Objects.
   	 * Song having (string, string) type from the class.
	 */
    
    public ArrayList<Song> getNewKidsOnTheBlocksSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                            		 //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Step By Step", "New Kids On The Block");        //Create a song
         Song track2 = new Song("Tonight", "New Kids On The Block");       		 //Create another song
         this.albumTracks.add(track1);                                    		 //Add the first song to song list for New Kids On The Block
         this.albumTracks.add(track2);                                    		 //Add the second song to song list for New Kids On The Block 
         return albumTracks;                                              		 //Return the songs for New Kids On The Block in the form of an ArrayList
    }
}
