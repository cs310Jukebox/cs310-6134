package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BillyJoel {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BillyJoel() {
    }
    
    public ArrayList<Song> getBillyJoelSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                  //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Travellin Prayer", "Billy Joel");             //Create a song
         Song track2 = new Song("Piano Man", "Billy Joel");                    //Create another song
         Song track3 = new Song("Aint No Crime", "Billy Joel");                //Create another song
         Song track4 = new Song("Youre My Home", "Billy Joel");                //Create another song
         Song track5 = new Song("The Ballad Of Billy The Kid", "Billy Joel");  //Create another song
         Song track6 = new Song("Worse Comes To Worst", "Billy Joel");         //Create another song
         Song track7 = new Song("Stop In Nevada", "Billy Joel");               //Create another song
         Song track8 = new Song("If Only I Had The Words", "Billy Joel");      //Create another song
         Song track9 = new Song("Somewhere Along The Line", "Billy Joel");     //Create another song
         Song track10 = new Song("Captain Jack", "Billy Joel");                 //Create another song
         this.albumTracks.add(track1);                                         //Add the first song to song list for Billy Joel
         this.albumTracks.add(track2);                                         //Add the second song to song list for Billy Joel
         this.albumTracks.add(track3);                                         //Add the third song to song list for Billy Joel
         this.albumTracks.add(track4);                                         //Add the fourth song to song list for Billy Joel
         this.albumTracks.add(track5);                                         //Add the fifth song to song list for Billy Joel
         this.albumTracks.add(track6);                                         //Add the sixth song to song list for Billy Joel
         this.albumTracks.add(track7);                                         //Add the seventh song to song list for Billy Joel
         this.albumTracks.add(track8);                                         //Add the eighth song to song list for Billy Joel
         this.albumTracks.add(track9);                                         //Add the ninth song to song list for Billy Joel
         this.albumTracks.add(track10);                                        //Add the tenth song to song list for Billy Joel
         return albumTracks;                                                   //Return the songs for Billy Joel in the form of an ArrayList
    }
}
