package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Volbeat {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Volbeat() {
    }
    
    public ArrayList<Song> getVolbeatSongs() {
    	
    	 /* Creates a data structure in order to store the songs of the band.*/
    	 albumTracks = new ArrayList<Song>();
    	 /* Initializes 3 songs.*/
    	 Song track1 = new Song("Lola Montez", "Volbeat");
         Song track2 = new Song("The Devil's Bleeding Crown", "Volbeat");
         Song track3 = new Song("A Warrior's Call", "Volbeat");
         /* Adds songs to the data structure. */
         this.albumTracks.add(track1);
         this.albumTracks.add(track2);
         this.albumTracks.add(track3);
         /* Returns data structure. */
         return albumTracks;
    }
}
