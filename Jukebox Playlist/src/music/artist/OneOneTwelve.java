package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class OneOneTwelve {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public OneOneTwelve() {
    }
    
    public ArrayList<Song> getOneOneTwelveSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                       	        //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Just a Little While", "112");		            //Create a song
         Song track2 = new Song("Now That We're Done", "112");			        //Create another song
         Song track3 = new Song("Cupid", "112");								//Create a third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for 112
         this.albumTracks.add(track2);                                          //Add the second song to song list for 112
         this.albumTracks.add(track3);											//Add the third song to the song list for 112
         return albumTracks;                                                    //Return the songs for 112 in the form of an ArrayList
    }
}
