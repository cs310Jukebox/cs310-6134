package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Maxwell {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Maxwell() {
    }
    
    public ArrayList<Song> getMaxwellsongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Ascension (Don't Ever Wonder", "Maxwell");     //Create a song
         Song track2 = new Song("Suitelady (The Proposal Jam)", "Maxwell");     //Create 2nd song
         Song track3 = new Song("Matrimony: Maybe You", "Maxwell");             //Create 3rd song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Maxwell
         this.albumTracks.add(track2);                                          //Add the second song to song list for Maxwell
         this.albumTracks.add(track3);                                          //Add the third song to song list for Maxwell
         return albumTracks;                                                    //Return the songs for Maxwell in the form of an ArrayList
    }
}