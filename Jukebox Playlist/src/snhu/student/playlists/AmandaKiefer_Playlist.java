package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class AmandaKiefer_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> skilletTracks = new ArrayList<Song>();
    Skillet skilletBand = new Skillet();
	
    skilletTracks = skilletBand.getSkilletSongs();
	
	playlist.add(skilletTracks.get(0));
	playlist.add(skilletTracks.get(1));
	playlist.add(skilletTracks.get(2)); 
	
	
    ImagineDragons imagineDragonsBand = new ImagineDragons();
	ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
    imagineDragonsTracks = imagineDragonsBand.getImagineDragonsSongs();
	

	playlist.add(imagineDragonsTracks.get(2));
	
	ArrayList<Song> BreakingBenjaminTracks = new ArrayList<Song>();
	BreakingBenjamin BreakingBenjaminBand = new BreakingBenjamin();
	BreakingBenjaminTracks = BreakingBenjaminBand.getBreakingBenjaminSongs();
	
	playlist.add(BreakingBenjaminTracks.get(0));
	playlist.add(BreakingBenjaminTracks.get(1));
	playlist.add(BreakingBenjaminTracks.get(2));
	
	ArrayList<Song> ACDCTracks = new ArrayList<Song>();
	ACDC ACDCBand = new ACDC();
	ACDCTracks = ACDCBand.getACDCSongs();
	
	playlist.add(ACDCTracks.get(0));
	
	
    return playlist;
	}
}
