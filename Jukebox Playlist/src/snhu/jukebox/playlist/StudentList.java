package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();
		
		//Added Will Medrano Gutshall as new student profile
		String WillMedranoGutshall = "WillMedranoGutshall";
		studentNames.add(WillMedranoGutshall);
		
		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);
		
		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);
		
		String StudentName3 = "CoreyCanfield";
		studentNames.add(StudentName3);
		
		String StudentName123 = "CarlChristensen";
		studentNames.add(StudentName123);
		
		String StudentName4 = "TestBrianRussell";
		studentNames.add(StudentName4);
		
		String MichaelSwift = "MichaelSwift";
		studentNames.add(MichaelSwift);
		
		//String StudentName3 = "TestBrianRussell";
//		studentNames.add(StudentName3);

		String StudentName5 = "LarryMcRoberts";
		studentNames.add(StudentName5);
		
		String StudentName6 = "HannahHollee";
		studentNames.add(StudentName6);
		
		//Added student Katelyn Trachsel.
		String StudentName7 = "KatelynTrachsel";
		studentNames.add(StudentName7);
		
		String StudentName8 = "AmandaKiefer";
		studentNames.add(StudentName8);
		
		String StudentName9 = "ChelseaLong"
		studentNames.add(StudentName9);
		
		//Creating a string new student profile and adding to ArrayList 
		String StudentLHoo = "Li Siang Hoo"; // Creating a new student profile name (Li Siang Hoo)  
		studentNames.add(StudentLHoo);		 // Adding "StudentLHoo" to the end of the list.
		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;
	
		switch(student) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());
			   return TestStudent1;
			   
		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			   
		   case "WillMedranoGutshall_Playlist":
			   WillMedranoGutshall_Playlist willMedranoGutshallPlaylist = new WillMedranoGutshall_Playlist();
			   Student WillMedranoGutshall = new Student("WillMedranoGutshall", willMedranoGutshallPlaylist.StudentPlaylist());
			   return WillMedranoGutshall;	   

			   
/* Commenting out these lines so application can run correctly.
 * Required class is missing.
 */
//		   case "CoreyCanfield_Playlist":
//			   CoreyCanfield_Playlist coreyCanfieldPlaylist = new CoreyCanfield_Playlist();
//			   Student CoreyCanfield = new Student("CoreyCanfield", coreyCanfieldPlaylist.StudentPlaylist());
//			   return CoreyCanfield;
	
			   
//			// add case HannahHollee_Playlist   
		   case "HannahHollee_Playlist":
			   HannahHollee_Playlist hannahHolleePlaylist = new HannahHollee_Playlist();
			   Student HannahHollee = new Student("HannahHollee", hannahHolleePlaylist.StudentPlaylist());
			   return HannahHollee;
			   
		   case "CarlChristensen_Playlist":
			   CarlChristensen_Playlist carlChristensenPlaylist = new CarlChristensen_Playlist();
			   Student CarlChristensen = new Student("CarlChristensen", carlChristensenPlaylist.StudentPlaylist());
			   return CarlChristensen;
			   
			   //Module 6 adding my case statement. 
		   case "KatelynTrachsel_Playlist":
			   KatelynTrachsel_Playlist KatelynTrachselPlaylist = new KatelynTrachsel_Playlist();
		   Student KatelynTrachsel = new Student("KatelynTrachsel", KatelynTrachselPlaylist.StudentPlaylist());
		   return KatelynTrachsel;
		   
		   case "MichaelSwift_Playlist":
			   MichaelSwift_Playlist MichaelSwiftPlaylist = new MichaelSwift_Playlist();
			   Student MichaelSwift = new Student("MichaelSwift", MichaelSwiftPlaylist.StudentPlaylist());
			   return MichaelSwift;
			   
		   case "AmandaKiefer_Playlist":
			   AmandaKiefer_Playlist AmandaKieferPlaylist = new AmandaKiefer_Playlist();
			   Student AmandaKiefer = new Student("AmandaKiefer", AmandaKieferPlaylist.StudentPlaylist());
			   return AmandaKiefer;
			   
		   case "ChelseaLong_Playlist":
			   ChelseaLong_Playlist ChelseaLongPlaylist = new ChelseaLong_Playlist();
			   Student ChelseaLong = new Student("ChelseaLong", ChelseaLongPlaylist.StudentPlaylist());
			   return ChelseaLong;

		   // Add Li Siang Hoo profile
		   case "LiSiangHoo_Playlist":
			   LiSiangHoo_Playlist liSiangHooPlaylist = new LiSiangHoo_Playlist();						//Create LiSiangHoo_Playlist object so we can access it's methods and properties
			   Student LiSiangHoo = new Student("Li Siang Hoo", liSiangHooPlaylist.StudentPlaylist());	//Create Student object
			   return LiSiangHoo;                                                                       //Return to Student (Li Siang Hoo)
		}
		
		return emptyStudent;
	}
}
