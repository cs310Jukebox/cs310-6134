package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GreenDay {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GreenDay() {
    }
    
    public ArrayList<Song> getGreenDaySongs() {
    	
    	 /* Creates a data structure in order to store the songs of the band.*/
    	 albumTracks = new ArrayList<Song>();
    	 /* Initializes 2 songs.*/
    	 Song track1 = new Song("21 Guns", "Green Day");
         Song track2 = new Song("American Idiot", "Green Day");
         /* Adds songs to the album. */
         this.albumTracks.add(track1);
         this.albumTracks.add(track2);
         /* Returns data structure. */
         return albumTracks;
    }
}
