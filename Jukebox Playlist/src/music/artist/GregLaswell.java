package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GregLaswell {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GregLaswell() {
    }
    
    public ArrayList<Song> getGregLaswellSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("And Then You", "Greg Laswell");                //Create a song
         Song track2 = new Song("Comes And Goes", "Greg Laswell");              //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Greg Laswell
         this.albumTracks.add(track2);                                          //Add the second song to song list for Greg Laswell
         return albumTracks;                                                    //Return the songs for Greg Laswell in the form of an ArrayList
    }
}