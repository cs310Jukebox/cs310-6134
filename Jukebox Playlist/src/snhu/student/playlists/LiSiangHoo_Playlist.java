package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class LiSiangHoo_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	//Artist: Madonna
	ArrayList<Song> madonnasTracks = new ArrayList<Song>(); 					//create ArrayList of song objects
	Madonna madonna = new Madonna(); 											//create the Madonna object so we can access it's methods and properties
	madonnasTracks = madonna.getMadonnasSongs(); 		     					//populate the madonnasTracks list with the actual values in the Madonna object
	//add three madonna song tracks to playlist
	playlist.add(madonnasTracks.get(0));
	playlist.add(madonnasTracks.get(1));
	playlist.add(madonnasTracks.get(2));
	
	//Artist: New Kids On The Block
	NewKidsOnTheBlock newKidsOnTheBlock = new NewKidsOnTheBlock();				//create the NewKidsOnTheBlock object so we can access it's methods and properties
	ArrayList<Song> newKidsOnTheBlocksTracks = new ArrayList<Song>();			//create ArrayList of song objects
	newKidsOnTheBlocksTracks = newKidsOnTheBlock.getNewKidsOnTheBlocksSongs();	//populate the newKidsOnTheBlocksTracks list with the actual values in the NewKidsOnTheBlock object
	//add two New Kids On The Block song tracks to playlist
   	playlist.add(newKidsOnTheBlocksTracks.get(0));
	playlist.add(newKidsOnTheBlocksTracks.get(1));
	
	//Artist: ACDC (From another student artist)
	ACDC ACDCBand = new ACDC();													//create the ACDCBand object so we can access it's methods and properties
	ArrayList<Song> ACDCsTracks = new ArrayList<Song>();						//create ArrayList of song objects
	ACDCsTracks = ACDCBand.getACDCSongs();										//populate the ACDCsTracks list with the actual values in the ACDCBand object
	//add three ACDC song tracks to playlist
	playlist.add(ACDCsTracks.get(0));
	playlist.add(ACDCsTracks.get(1));	
	playlist.add(ACDCsTracks.get(2));
	
    return playlist;
	}
}
