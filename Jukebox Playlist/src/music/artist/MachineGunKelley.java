package music.artist;

//This Playlist Added by Will Medrano Gutshall



import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class MachineGunKelley {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public MachineGunKelley() {
    }
    
    public ArrayList<Song> getMachineGunKelleySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Rap Devil", "Machine Gun Kelley");             //Create a song
         Song track2 = new Song("Till I Die", "Machine Gun Kelley");            //Create 2nd song
         Song track3 = new Song("Cleveland", "Machine Gun Kelley");             //Create 3rd song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the MGK
         this.albumTracks.add(track2);                                          //Add the second song to song list for the MGK
         this.albumTracks.add(track3);                                          //Add the third song to song list for the MKG 
         return albumTracks;                                                    //Return the songs for the MGK in the form of an ArrayList
    }
}
