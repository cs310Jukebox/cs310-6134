package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/**
 * Madonna.java
 * 
 * This class represent the Madonna song lists
 * 
 * @author Li Siang Hoo
 *
 */

public class Madonna {
	
	ArrayList<Song> albumTracks; // ArrayList of album tracks
    String albumTitle;			 // Album title
    
    public Madonna() {
    }
    
    /*
   	 * @return ArrayList
   	 * This returns to list of storing the Song Objects.
   	 * Song having (string, string) type from the class.
   	 */
    
    public ArrayList<Song> getMadonnasSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                             //Instantiate the album so we can populate it below
    	 Song track1 = new Song("La Isla Bonita", "Madonna");             //Create a song
         Song track2 = new Song("Batuka", "Madonna");       			  //Create another song
         Song track3 = new Song("Frozen", "Madonna");       			  //Create third song
         this.albumTracks.add(track1);                                    //Add the first song to song list for Madonna
         this.albumTracks.add(track2);                                    //Add the second song to song list for Madonna 
         this.albumTracks.add(track3);  								  //Add the third song to song list for Madonna
         return albumTracks;                                              //Return the songs for Madonna in the form of an ArrayList
    }
}
