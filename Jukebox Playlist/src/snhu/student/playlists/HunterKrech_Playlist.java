package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class HunterKrech_Playlist {
	public LinkedList<PlayableSong> StudentPlaylist(){
		
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	//Initializing array lists  for tracks
	ArrayList<Song> nirvanaTracks = new ArrayList<Song>();
	ArrayList<Song> BeeGeesTracks = new ArrayList<Song>();
	ArrayList<Song> QueenTracks = new ArrayList<Song>();
	ArrayList<Song> ACDCTracks = new ArrayList<Song>();
	
    Nirvana nirvanaBand = new Nirvana();
    BeeGees BeeGeesBand = new BeeGees();
    Queen QueenBand = new Queen();
    ACDC ACDCBand = new ACDC();
	
    //Function call to artist's class
    nirvanaTracks = nirvanaBand.getNirvanaSongs();
    BeeGeesTracks = BeeGeesBand.getBeeGeesSongs();
    QueenTracks = QueenBand.getQueenSongs();
    ACDCTracks = ACDCBand.getACDCSongs();
	
    //Adding songs to playlist
	playlist.add(nirvanaTracks.get(0));
	playlist.add(nirvanaTracks.get(1));
	
	//Classmate artist 1
	playlist.add(BeeGeesTracks.get(0));
	playlist.add(BeeGeesTracks.get(1));
	
	//Classmate artist 2
	playlist.add(QueenTracks.get(0));
	playlist.add(QueenTracks.get(1));
	
	//Classmate artist 3
	playlist.add(ACDCTracks.get(0));
	playlist.add(ACDCTracks.get(1));
	
	
    return playlist;
	}
}