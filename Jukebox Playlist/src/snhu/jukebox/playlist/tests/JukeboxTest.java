package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	

	// Test Code for Machine Gun Kelley
		// Added by Will Medrano Gutshall
		@Test
		public void testGetMachineGunKelleyAlbumSize() throws NoSuchFieldException, SecurityException {
			 MachineGunKelley machinegunkelley = new MachineGunKelley();
			 ArrayList<Song> MachineGunKelleyTracks = new ArrayList<Song>();
			 MachineGunKelleyTracks = machinegunkelley.getMachineGunKelleySongs();
			 assertEquals(3, MachineGunKelleyTracks.size());
		}
		
		// Test Code for BoneThugs
		// Added by Will Medrano Gutshall
		@Test
		public void testBoneThugsAlbumSize() throws NoSuchFieldException, SecurityException {
			BoneThugs bonethugs = new BoneThugs();
			ArrayList<Song> BoneThugsTracks = new ArrayList<Song>();
			BoneThugsTracks = bonethugs.getBoneThugsSongs();
			assertEquals(3, BoneThugsTracks.size());
		}

/*
 * Commenting out this test as it fails
 * Larry McRoberts
 */
//	@Test
//	public void testGetGreenDayAlbumSize() throws NoSuchFieldException, SecurityException {
//		 GreenDay gd = new GreenDay();
//		 ArrayList<Song> gdsTracks = new ArrayList<Song>();
//		 gdsTracks = gd.getGreenDaySongs();
//		 assertEquals(3, gdsTracks.size());
//	}
	
	@Test
	public void testGetVolbeatAlbumSize() throws NoSuchFieldException, SecurityException {
		 Volbeat v = new Volbeat();
		 ArrayList<Song> vsTracks = new ArrayList<Song>();
		 vsTracks = v.getVolbeatSongs();
		 assertEquals(3, vsTracks.size());
	}
	
	
	@Test
	public void testGetSkilletAlbumSize() throws NoSuchFieldException, SecurityException {
		 Skillet skilletBand = new Skillet();
		 ArrayList<Song> skilletTracks = new ArrayList<Song>();
		 skilletTracks = skilletBand.getSkilletSongs();
		 assertEquals(3, skilletTracks.size());
	}
	
	@Test
	public void testGetBreakingBenjaminAlbumSize() throws NoSuchFieldException, SecurityException {
		BreakingBenjamin BreakingBenjaminBand = new BreakingBenjamin();
		 ArrayList<Song> BreakingBenjaminTracks = new ArrayList<Song>();
		 BreakingBenjaminTracks = BreakingBenjaminBand.getBreakingBenjaminSongs();
		 assertEquals(3, BreakingBenjaminTracks.size());
	}

	@Test
	public void testGetMadonnasAlbumSize() throws NoSuchFieldException, SecurityException {
		 Madonna madonna = new Madonna();												//instantiating the Madonna object so we can access it's methods and properties
		 ArrayList<Song> madonnasTracks = new ArrayList<Song>();						//create ArrayList of song objects
		 madonnasTracks = madonna.getMadonnasSongs();									//populate the madonnasTracks list with the actual values in the Madonna object
		 assertEquals(3, madonnasTracks.size());										//test case to checking of the size the madonnasTracks list 
	}
	
	@Test
	public void testGetNewKidsOnTheBlocskAlbumSize() throws NoSuchFieldException, SecurityException {
		 NewKidsOnTheBlock newKidsOnTheBlock = new NewKidsOnTheBlock();					//instantiating the NewKidsOnTheBlock object so we can access it's methods and properties
		 ArrayList<Song> newKidsOnTheBlocksTracks = new ArrayList<Song>();				//create ArrayList of song objects
		 newKidsOnTheBlocksTracks = newKidsOnTheBlock.getNewKidsOnTheBlocksSongs();		//populate the newKidsOnTheBlocksTracks list with the actual values in the NewKidsOnTheBlock object
		 assertEquals(2, newKidsOnTheBlocksTracks.size());								//test case to checking of the size the newKidsOnTheBlocksTracks list 
	}
	

}
