package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class ChelseaLong_Playlist {
	
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		/* Creates data structure to store songs in a playlist. */
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> MaxwellTracks = new ArrayList<Song>();
		Maxwell maxwell = new maxwell();
		
		//Obtain songs for playlist from Maxwell class
	    MaxwellTracks = maxwell.getMaxwellSongs();
		
		playlist.add(MaxwellTracks.get(0));
		playlist.add(MaxwellTracks.get(1));
		playlist.add(MaxwellTracks.get(2));
		
		//Structure for ToniBraxton
	    ToniBraxton ToniBraxtonband = new ToniBraxton();
		ArrayList<Song> ToniBraxtonTracks = new ArrayList<Song>();
	    ToniBraxtonTracks = ToniBraxtonband.getToniBraxtonSongs();
		
	    //Obtain songs for playlist from ToniBraxton class
		playlist.add(ToniBraxtonTracks.get(0));
		playlist.add(ToniBraxtonTracks.get(1));
		
		
		//Structure for Adele
		Adele AdeleBand = new Adele();
		ArrayList<Song> AdeleTracks = new ArrayList<Song>();
		AdeleTracks = AdeleBand.getAdeleSongs();
		
		//Obtain songs from Adele class
		playlist.add(AdeleTracks.get(0));
		playlist.add(AdeleTracks.get(1));
		playlist.add(AdeleTracks.get(2));
	    
		//Structure for BoneThugs
		BoneThugs BoneThugsBand = new BoneThugs();
		ArrayList<Song> BoneThugsTracks = new ArrayList<Song>();
		BoneThugsTracks = BoneThugsBand.getBoneThugsSongs();
		
		//Obtain songs from BoneThugs class
		playlist.add(BoneThugsTracks.get(0));
		playlist.add(BoneThugsTracks.get(1));
		playlist.add(BoneThugsTracks.get(2));
				
		return playlist;
		}

}
