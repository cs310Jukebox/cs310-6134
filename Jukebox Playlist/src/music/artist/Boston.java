package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Boston {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Boston() {
    }
    
    public ArrayList<Song> getBostonSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                              //Instantiate the album so we can populate it below
    	 Song track1 = new Song("More Than A Feeling", "Boston");          //Create a song
         Song track2 = new Song("Peace of Mind", "Boston");                //Create another song
         Song track3 = new Song("Foreplay, Long Time", "Boston");          //Create another song
         Song track4 = new Song("Rock & Roll Band", "Boston");             //Create another song
         Song track5 = new Song("Smokin'", "Boston");                      //Create another song
         Song track6 = new Song("Hitch A Ride", "Boston");                 //Create another song
         Song track7 = new Song("Something About You", "Boston");          //Create another song
         Song track8 = new Song("Let Me Take You Home Tonight", "Boston"); //Create another song
         this.albumTracks.add(track1);                                     //Add the first song to song list for Boston
         this.albumTracks.add(track2);                                     //Add the second song to song list for Boston 
         this.albumTracks.add(track3);                                     //Add the third song to song list for Boston 
         this.albumTracks.add(track4);                                     //Add the fourth song to song list for Boston 
         this.albumTracks.add(track5);                                     //Add the fifth song to song list for Boston 
         this.albumTracks.add(track6);                                     //Add the sixth song to song list for Boston 
         this.albumTracks.add(track7);                                     //Add the seventh song to song list for Boston 
         this.albumTracks.add(track8);                                     //Add the eighth song to song list for Boston 
         return albumTracks;                                               //Return the songs for Boston in the form of an ArrayList
    }
}
