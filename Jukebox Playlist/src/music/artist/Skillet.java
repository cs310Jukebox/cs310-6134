package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Skillet {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Skillet() {
    }
    
    public ArrayList<Song> getSkilletSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Hero", "Skillet");            					//Create first song
         Song track2 = new Song("Monster", "Skillet");         					//Create second song
         Song track3 = new Song("Legendary", "Skillet");						//Create third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Skillet
         this.albumTracks.add(track2);                                          //Add the second song to song list for Skillet 
         this.albumTracks.add(track3);											//Add the third song to song list for Skillet
         return albumTracks;                                                    //Return the songs for Skillet in the form of an ArrayList
    }
}